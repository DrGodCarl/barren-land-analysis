use anyhow::{anyhow, Result};
use std::collections::HashSet;
use std::io;

extern crate anyhow;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;

    let barren_areas = parse(input)?;
    let farm = Farm::default_with(barren_areas);
    let results = farm.find_fertile_areas();
    let result = results
        .iter()
        .map(|n| n.to_string())
        .collect::<Vec<_>>()
        .join(" ");
    println!("{}", result);
    Ok(())
}

fn parse(input: String) -> Result<Vec<BarrenArea>> {
    let input = input.trim();
    if !input.starts_with("{") || !input.ends_with("}") {
        return Err(anyhow!("Missing prefix or suffix in input: {}.", input));
    }
    let boxes_input = input
        .trim_start_matches("{")
        .trim_end_matches("}")
        .split(",");
    return boxes_input.into_iter().map(|i| parse_one(i)).collect();
}

fn parse_one(box_input: &str) -> Result<BarrenArea> {
    let box_input = box_input.trim();
    // Handle smart quotes, since the doc has them.
    if !(box_input.starts_with("\"") || box_input.starts_with("“"))
        || !(box_input.ends_with("\"") || box_input.ends_with("”"))
    {
        return Err(anyhow!("Missing proper quotes around: {}", box_input));
    }

    let numbers = box_input
        .trim_start_matches(|c| c == '\"' || c == '“')
        .trim_end_matches(|c| c == '\"' || c == '”')
        .split(" ")
        .map(|s| s.parse::<u32>())
        .filter_map(Result::ok)
        .collect::<Vec<_>>();

    if let [bottom_left_x, bottom_left_y, top_right_x, top_right_y] = numbers.as_slice() {
        return Ok(BarrenArea {
            bottom_left: Point::of(*bottom_left_x, *bottom_left_y),
            top_right: Point::of(*top_right_x, *top_right_y),
        });
    } else {
        return Err(anyhow!(
            "Improper number of numerical arguments: {}",
            box_input
        ));
    }
}

#[derive(Hash, Eq, PartialEq, Debug, Clone, Copy)]
pub struct Point {
    pub x: u32,
    pub y: u32,
}

impl Point {
    fn of(x: u32, y: u32) -> Self {
        Point { x, y }
    }
}

#[derive(Debug)]
pub struct BarrenArea {
    pub bottom_left: Point,
    pub top_right: Point,
}

trait Container {
    fn contains(&self, point: &Point) -> bool;
}

impl Container for BarrenArea {
    fn contains(&self, point: &Point) -> bool {
        point.x >= self.bottom_left.x
            && point.x <= self.top_right.x
            && point.y >= self.bottom_left.y
            && point.y <= self.top_right.y
    }
}

impl Container for Vec<BarrenArea> {
    fn contains(&self, point: &Point) -> bool {
        self.iter().any(|a| a.contains(point))
    }
}

pub struct Farm {
    pub length: u32,
    pub width: u32,
    pub barren_areas: Vec<BarrenArea>,
}

impl Farm {
    fn default_with(barren_areas: Vec<BarrenArea>) -> Self {
        Farm {
            length: 400,
            width: 600,
            barren_areas,
        }
    }

    fn find_fertile_areas(&self) -> Vec<u32> {
        let mut visited = HashSet::new();
        let mut result = vec![];
        for x in 0..self.length {
            for y in 0..self.width {
                let point = Point::of(x, y);

                let fertile_area = self.explore_land(point, &mut visited);
                if let Some(area) = fertile_area {
                    result.push(area)
                }
            }
        }
        result.sort();
        result
    }

    fn explore_land(&self, starting_point: Point, visited: &mut HashSet<Point>) -> Option<u32> {
        let mut area = None;

        let mut to_explore = vec![starting_point];
        while let Some(current) = to_explore.pop() {
            if visited.contains(&current) || self.barren_areas.contains(&current) {
                continue;
            }

            area = Some(area.map_or_else(|| 1, |a| a + 1));
            visited.insert(current);
            self.north_of(&current).map(|p| to_explore.push(p));
            self.east_of(&current).map(|p| to_explore.push(p));
            self.south_of(&current).map(|p| to_explore.push(p));
            self.west_of(&current).map(|p| to_explore.push(p));
        }
        return area;
    }

    fn north_of(&self, point: &Point) -> Option<Point> {
        if point.y == 0 {
            return None;
        }

        return Some(Point::of(point.x, point.y - 1));
    }

    fn east_of(&self, point: &Point) -> Option<Point> {
        if point.x == self.length - 1 {
            return None;
        }

        return Some(Point::of(point.x + 1, point.y));
    }

    fn south_of(&self, point: &Point) -> Option<Point> {
        if point.y == self.width - 1 {
            return None;
        }

        return Some(Point::of(point.x, point.y + 1));
    }

    fn west_of(&self, point: &Point) -> Option<Point> {
        if point.x == 0 {
            return None;
        }

        return Some(Point::of(point.x - 1, point.y));
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_contains() {
        let area = BarrenArea {
            bottom_left: Point::of(3, 3),
            top_right: Point::of(6, 6),
        };
        assert!(area.contains(&Point::of(4, 4)));
        assert!(area.contains(&Point::of(6, 6)));
        assert!(area.contains(&Point::of(3, 3)));
        assert!(!area.contains(&Point::of(3, 2)));
        assert!(!area.contains(&Point::of(7, 6)));
    }

    #[test]
    fn test_contains_collection() {
        let areas = vec![BarrenArea {
            bottom_left: Point::of(3, 3),
            top_right: Point::of(6, 6),
        }];
        assert!(areas.contains(&Point::of(4, 4)));
        assert!(areas.contains(&Point::of(6, 6)));
        assert!(areas.contains(&Point::of(3, 3)));
        assert!(!areas.contains(&Point::of(3, 2)));
        assert!(!areas.contains(&Point::of(7, 6)));
    }

    #[test]
    fn test_find_fertile_areas_0() {
        let barren_areas = vec![BarrenArea {
            bottom_left: Point::of(0, 3),
            top_right: Point::of(3, 4),
        }];
        let farm = Farm {
            length: 4,
            width: 6,
            barren_areas: barren_areas,
        };
        let result = farm.find_fertile_areas();

        assert_eq!(result, vec![4, 12]);
    }

    #[test]
    fn test_find_fertile_areas_1() {
        let barren_areas = vec![BarrenArea {
            bottom_left: Point::of(0, 292),
            top_right: Point::of(399, 307),
        }];
        let farm = Farm::default_with(barren_areas);
        let result = farm.find_fertile_areas();

        assert_eq!(result, vec![116800, 116800]);
    }

    #[test]
    fn test_find_fertile_areas_2() {
        let barren_areas = vec![
            BarrenArea {
                bottom_left: Point::of(48, 192),
                top_right: Point::of(351, 207),
            },
            BarrenArea {
                bottom_left: Point::of(48, 392),
                top_right: Point::of(351, 407),
            },
            BarrenArea {
                bottom_left: Point::of(120, 52),
                top_right: Point::of(135, 547),
            },
            BarrenArea {
                bottom_left: Point::of(260, 52),
                top_right: Point::of(275, 547),
            },
        ];
        let farm = Farm::default_with(barren_areas);
        let result = farm.find_fertile_areas();

        assert_eq!(result, vec![22816, 192608]);
    }
}
